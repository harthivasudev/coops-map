#!/usr/bin/env python3

import json
import sqlite3
from flask import Flask, render_template, request, jsonify

app = Flask(__name__)
db_path = 'data.db'


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/api/coops/', methods=['GET', 'POST'])
def all_coops():
    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    if request.method == 'GET':
        cursor.execute("SELECT name, lat, lon, id FROM coops where reviewed='1'")
        rows = cursor.fetchall()

        dict_data = {}

        for row in rows:
            dict_data[row[3]] = {'name': row[0], 'lat': row[1], 'lon': row[2]}

        data = jsonify({'data': dict_data}), 200
        # return json data

    elif request.method == 'POST':
        try:
            name = request.form.get('name') if request.form.get('name') else None
            lat = request.form.get('lat') if request.form.get('lat') else None
            lon = request.form.get('lon') if request.form.get('lon') else None
            coop_type = request.form.get('type') if request.form.get('type') else None
            desc = request.form.get('desc') if request.form.get('desc') else None
            phone = request.form.get('phone') if request.form.get('phone') else None
            address = request.form.get('address') if request.form.get('address') else None
            website = request.form.get('website') if request.form.get('website') else None
            coop_open = request.form.get('open') if request.form.get('open') else None

            # handle request data
            # process and push into db
            record = (name, desc, coop_type, lat, lon, phone, address, website, coop_open, 1)
            cursor.execute("INSERT INTO coops values(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", record)
            connection.commit()

            data = jsonify({"message": "Kudos! Coop added! Refresh to see."}), 201
            # return message
        except sqlite3.IntegrityError as ie:
            print(ie)
            error_field = ie.args[0].split(":")[1].split('.')[1]
            data = jsonify({"message": "cannot be empty", "field": error_field}), 400
        except:
            print("Unknwon exception occured")

    cursor.close()
    connection.close()
    # return data in json format
    return data


@app.route('/api/coops/<id>', methods=['GET'])
def one_coops(id):
    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    if request.method == 'GET':
        cursor.execute("SELECT * FROM coops where id=%d" % int(id))

        row = cursor.fetchone()
        dict_data = {'name': row[1], 'desc': row[2], 'type': row[3],
                'lat': row[4], 'lon': row[5], 'phone': row[6],
                'address': row[7], 'website': row[8], 'open': row[9]
                }
        data = jsonify({'data': dict_data}), 200

    cursor.close()
    connection.close()
    # return data in json format
    return data


if __name__ == '__main__':
    app.run(debug=False, port=7000)
