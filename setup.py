#!/usr/bin/env python3

import sqlite3

if __name__ == "__main__":
    connection = sqlite3.connect('data.db')
    cursor = connection.cursor()

    # TABLE CREATE
    create_query = "CREATE TABLE coops (id INTEGER PRIMARY KEY AUTOINCREMENT, \
                    name TEXT NOT NULL, desc TEXT NOT NULL, type CHAR(255), \
                    lat CHAR(10) NOT NULL, lon CHAR(10) NOT NULL, phone CHAR(15), \
                    address TEXT, website TEXT, open TEXT, reviewed BOOLEAN);"
    cursor.execute(create_query)

    connection.commit()
    cursor.close()
    connection.close()
