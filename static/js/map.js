var mymap = L.map('mapid').setView([12.200, 78.871], 7);
var new_marker = null;
var marker = null;

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mymap);


function onMapClick(e) {
    if (new_marker != null) {
        mymap.removeLayer(new_marker);
    }

    // prepare a marker to display
    new_marker = L.marker(e.latlng, {draggable: false});

    // prepare a popup to display
    popup = L.popup();
    popup.setContent("You clicked here");

    // bind the popup with marker
    new_marker.bindPopup(popup).openPopup();
    // add the marker to map
    new_marker.addTo(mymap);

    // set the caption
    document.getElementById('add-place').getElementsByTagName('h2')[0].innerHTML = "Mark a Coop.";

    // reset form fields due to previous action
    document.forms['mark-a-coop'].reset();

    // fill the lat and lng in form
    document.getElementById('lat').value = e.latlng.lat;
    document.getElementById('lon').value = e.latlng.lng;

    // enable form fields if disabled by previous action
    form_elements = document.forms['mark-a-coop'].elements;
    for (i=0; i<form_elements.length; ++i){
        if (form_elements[i].readOnly === true)
            form_elements[i].readOnly = false;
    }

    // bring up the from to front
    form_div = document.getElementById('add-place');
    form_div.style.display = "block";
    console.log("testing");

    map_div = document.getElementById('mapid');
    map_div.style.width = "85%";

    // toggle action buttons
    document.getElementById('action-btns').style.display = "block";
    document.getElementById('edit-btns').style.display = "none";
}

mymap.on('click', onMapClick);
