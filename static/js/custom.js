document.addEventListener("DOMContentLoaded", function(event) {
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest();
    }

    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                resp = JSON.parse(httpRequest.responseText);
                for (key in resp.data) {
                    marker = L.marker({'lat': resp.data[key].lat, 'lng': resp.data[key].lon});

                    popup_content = "<a href='#' onClick='showDetails(" + key + ")'>" + resp.data[key].name + "</a>";
                    old_popup = L.popup();
                    old_popup.setContent(popup_content);

                    marker.bindPopup(old_popup).openPopup();
                    marker.addTo(mymap);
                }
            }
        }
    };

    httpRequest.open('GET', '/api/coops/', true);
    httpRequest.send();
});


function showDetails() {
    console.log("id is " + arguments[0]);
    if (window.XMLHttpRequest){
        ajaxRequest = new XMLHttpRequest();
    }

    ajaxRequest.onreadystatechange = function(){
        if (ajaxRequest.readyState === XMLHttpRequest.DONE) {
            if (ajaxRequest.status === 200) {
                resp = JSON.parse(ajaxRequest.responseText);
                 form = document.forms['mark-a-coop'];
                 form_div = document.getElementById('add-place');
                 map_div = document.getElementById('mapid');

                // disable form fields
                form_elements = form.elements;
                for (i=0; i<form_elements.length; ++i){
                    form_elements[i].readOnly = true;
                }

                // hide add & cancel button
                action_div = document.getElementById('action-btns');
                if (action_div.style.display !== "none")
                    action_div.style.display = "none";

                // show edit actions
                edit_div = document.getElementById('edit-btns');
                if (edit_div.style.display !== "block")
                    edit_div.style.display = "block";

                // change heading
                caption = form_div.getElementsByTagName('h2')[0];
                caption.innerHTML = resp.data['name']

                // [TODO]show edit button (not until server side is implemented)

                for (key in resp.data) {
                    // populate the form with existing data
                    form[key].value = resp.data[key];

                }

                // bring back the side info section
                if (form_div.style.display !== "block") {
                    form_div.style.display = "block";
                        map_div.style.width = "85%";
                }

            }
        }
    };

    ajaxRequest.open('GET', '/api/coops/'+arguments[0], true);
    ajaxRequest.send();
}

function submitData(){
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest();
    }

    form = document.forms['mark-a-coop'];
    data = new FormData(form);

    httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState === XMLHttpRequest.DONE){
            if(httpRequest.status === 201) {
                // reset form
                document.getElementById('mark-a-coop').reset();

                // clear formatting
                document.getElementById('error').innerHTML = "";
                previous_target = document.getElementsByClassName('error_input');
                if (previous_target.length !== 0){
                    previous_target[0].classList.remove('error_input');
                }

                // remove marker
                mymap.removeLayer(new_marker);

                // make form hidden and expand map size
                form_div.style.display = "none";
                map_div.style.width = "100%";

                alert(JSON.parse(httpRequest.responseText).message);
            } else if (httpRequest.status === 400) {
                //clear formatting for previous error message
                previous_target = document.getElementsByClassName('error_input')
                if (previous_target.length !== 0) {
                    previous_target[0].classList.remove('error_input');
                }

                resp = JSON.parse(httpRequest.responseText);
                document.getElementById('error').innerHTML = resp.message;

                target_element = document.getElementsByName(resp.field)[0]
                target_element.classList.add('error_input');
                target_element.focus();
            }
        }
    };

    httpRequest.open('POST', '/api/coops/', true);
    //httpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    httpRequest.send(data);
}


function cancelOp(){
    mymap.removeLayer(new_marker);
    resetView();
}

function resetView(){
    document.getElementById('add-place').getElementsByTagName('h2')[0].innerHTML = "Mark a Coop."
    document.getElementById('mark-a-coop').reset();
    form_div.style.display = "none";
    map_div.style.width = "100%";
}
